FROM python:3.8-alpine

RUN apk --no-cache --virtual .build add build-base libffi-dev && \
    pip install bottle gunicorn gevent prometheus_client && \
    apk del .build

WORKDIR /
ADD *.py /

USER 1000

CMD ["python", "/kubeless.py"]
