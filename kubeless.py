#!/usr/bin/env python

import os
import math
from importlib.machinery import SourceFileLoader

import gevent
import bottle
import prometheus_client as prom

func = getattr(SourceFileLoader('function',
                                '{}/{}.py'.format(os.getenv('KUBELESS_INSTALL_VOLUME'),
                                                  os.getenv('MOD_NAME'))).load_module(),
               os.getenv('FUNC_HANDLER'))
func_port = os.getenv('FUNC_PORT', 8080)
timeout = float(os.getenv('FUNC_TIMEOUT', 180))
workers = int(os.getenv('GUNICORN_WORKERS', math.ceil(os.cpu_count() / 2)))
worker_class = os.getenv('GUNICORN_WORKER_CLASS', 'gevent')

app = application = bottle.app()

func_hist = prom.Histogram('function_duration_seconds',
                           'Duration of user function in seconds',
                           ['method'])
func_calls = prom.Counter('function_calls_total',
                           'Number of calls to user function',
                          ['method'])
func_errors = prom.Counter('function_failures_total',
                           'Number of exceptions in user function',
                           ['method'])

function_context = {
    'function-name': func,
    'timeout': timeout,
    'runtime': os.getenv('FUNC_RUNTIME'),
    'memory-limit': os.getenv('FUNC_MEMORY_LIMIT'),
}


@app.get('/healthz')
def healthz():
    return 'OK'


@app.get('/metrics')
def metrics():
    bottle.response.content_type = prom.CONTENT_TYPE_LATEST
    return prom.generate_latest(prom.REGISTRY)


@app.route('/<:re:.*>', method=['GET', 'POST', 'PATCH', 'DELETE'])
def handler():
    req = bottle.request
    res = bottle.response
    content_type = req.get_header('content-type')
    data = req.body.read()
    if content_type == 'application/json':
        data = req.json
    event = {
        'data': data,
        'event-id': req.get_header('event-id'),
        'event-type': req.get_header('event-type'),
        'event-time': req.get_header('event-time'),
        'event-namespace': req.get_header('event-namespace'),
        'extensions': {
            'request': req.copy(),
        }
    }
    method = req.method
    func_calls.labels(method).inc()
    with func_errors.labels(method).count_exceptions():
        with func_hist.labels(method).time():
            task = gevent.spawn(func, event, function_context)
            try:
                result = task.get(timeout=timeout)
                if isinstance(result, tuple):
                    result_length = len(result)
                    if result_length > 2:
                        option = result[2]
                        if isinstance(option, bottle.HeaderDict):
                            for header in option.allitems():
                                res.headers.append(*header)
                        elif isinstance(option, bottle.MultiDict):
                            for key, val in option.allitems():
                                if hasattr(res, key):
                                    f = getattr(res, key)
                                    if isinstance(val, tuple):
                                        f(*val)
                                    elif isinstance(val, dict):
                                        f(**val)
                                    else:
                                        f(val)
                    if result_length > 1:
                        res.status = result[1]
                    return result[0]
                return result
            except gevent.Timeout:
                task.kill(block=False)
                gevent.sleep(-1)
                return bottle.HTTPError(408, 'Timeout while processing the function')


if __name__ == '__main__':
    bottle.run(app, server='gunicorn', host='0.0.0.0', port=func_port, timeout=int(timeout + 1),
               workers=workers, worker_class=worker_class, logger_class='log.Logger', accesslog='-',
               access_log_format='%(h)s %(l)s %(u)s %(t)s “%(r)s” %(s)s %(b)s “%(f)s” “%(a)s” %(T)s')
